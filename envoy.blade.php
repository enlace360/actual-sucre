@setup
$site_name = 'actual';
$ssh_user = 'forge';
$server_ip = '167.99.150.146';
$server_base_path = '/home/forge/actual.digitalpartner.cl';
$sync_folder =  '/storage/app/';

$ssh_connection = $ssh_user.'@'.$server_ip;
@endsetup

@servers(['server' => [$ssh_connection], 'local' => '127.0.0.1'])

@task('setup', ['on' => 'local'])
echo "<info>Installing composer dependencies...</info>";
composer install --prefer-dist
echo "<info>Installing NPM dependencies...</info>";
npm install
echo "<info>Compiling assets...</info>";
npm run watch
@endtask

@story('sync')
db:backup
db:update_local
db:delete_dump_server
db:delete_dump_local
pull_images
@endstory

@story('sync_database')
db:backup
db:update_local
db:delete_dump_server
db:delete_dump_local
@endstory

@task('show_env', ['on' => 'server'])
cat {{ $server_base_path }}/.env
@endtask

@task('db:backup', ['on' => 'server'])
cd {{ $server_base_path }}

export DB_HOST=$(php vendor/bin/envoy_helper get_env DB_HOST)
export DB_PASS=$(php vendor/bin/envoy_helper get_env DB_PASSWORD)
export DB_USERNAME=$(php vendor/bin/envoy_helper get_env DB_USERNAME)
export DB_DATABASE=$(php vendor/bin/envoy_helper get_env DB_DATABASE)

mysqldump -h $DB_HOST -u $DB_USERNAME -p$DB_PASS -c -e --default-character-set=utf8 --single-transaction --skip-set-charset --add-drop-database $DB_DATABASE | gzip -c > {{ $site_name }}.sql.gz
echo "Database dumped on server"
@endtask

@task('db:update_local', ['on' => 'local'])
if [ -f {{ $site_name }}.sql ]; then
echo "File {{ $site_name }}.sql deleted"
rm {{ $site_name }}.sql
fi

if [ -f {{ $site_name }}.sql.gz ]; then
echo "File {{ $site_name }}.sql.gz deleted"
rm {{ $site_name }}.sql.gz
fi

echo "Downloading database backup from server..."
rsync -avzP {{ $ssh_connection }}:{{ $server_base_path }}/{{ $site_name }}.sql.gz ./
echo "Database dump downloaded";
gzip -d {{ $site_name }}.sql.gz

echo "Importing backup in local database..."
export DB_HOST=$(php vendor/bin/envoy_helper get_env DB_HOST)
export DB_PASS=$(php vendor/bin/envoy_helper get_env DB_PASSWORD)
export DB_USERNAME=$(php vendor/bin/envoy_helper get_env DB_USERNAME)
export DB_DATABASE=$(php vendor/bin/envoy_helper get_env DB_DATABASE)

if [ -z "$DB_PASS" ]; then
echo "importing without mysql password"
mysql -h $DB_HOST -u $DB_USERNAME $DB_DATABASE < {{ $site_name }}.sql --binary-mode
else
mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASS $DB_DATABASE < {{ $site_name }}.sql --binary-mode
fi
echo "<info>Local database updated</info>"
@endtask

@task('db:delete_dump_server', ['on' => 'server'])
cd {{ $server_base_path }}
if [ -f {{ $site_name }}.sql.gz ]; then
rm {{ $site_name }}.sql.gz
fi
echo "Database dump deleted"
@endtask


@task('db:delete_dump_local', ['on' => 'local'])
if [ -f {{ $site_name }}.sql ]; then
rm {{ $site_name }}.sql
fi
echo "Database dump deleted from local"
@endtask

@task('pull_images', ['on' => 'local'])
echo "Syncing images..."
rsync -avzP --exclude=bfi_thumb  --exclude=backup-temp/* --exclude=*.zip --exclude=cache {{ $ssh_connection }}:{{ $server_base_path }}{{ $sync_folder }} .{{ $sync_folder }}
echo "Images Synced"
@endtask
