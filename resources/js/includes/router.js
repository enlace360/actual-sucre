import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/categories'
        },
        {
            path: '/categories',
            name: 'categories.index',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './../components/Categories.vue'),
        },
        {
            path: '/categories/:id',
            name: 'categories.show',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './../components/Category.vue'),
        },
        {
            path: '/tv',
            name: 'tv.show',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './../components/FullScreen.vue'),
        },
    ],
});
