<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{ asset('images/apple.png') }}">
    <link rel="apple-touch-startup-image" href="{{ asset('images/launcher-apple.png') }}">
    <meta name="apple-mobile-web-app-title" content="Actual Control Remoto">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">


    <title>{{ env('APP_NAME') }}</title>
</head>
<body>
<div id="app"></div>

<script>
    window.SOCKET_IO_URL = '{{ config('app.socket_io_url') }}';
</script>
<script src="{{mix('js/app.js')}}"></script>
</body>
</html>
