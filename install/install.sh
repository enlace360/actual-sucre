sudo sed -i "s/#precedence ::ffff:0:0\/96  100/precedence ::ffff:0:0\/96  100/" /etc/gai.conf

# Upgrade The Base Packages
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade -y

apt-get install -y --force-yes software-properties-common

# Add A Few PPAs To Stay Current
pt-add-repository ppa:nginx/development -y
apt-add-repository ppa:chris-lea/redis-server -y
apt-add-repository ppa:ondrej/apache2 -y
apt-add-repository ppa:ondrej/php -y

# Update Package Lists
apt-get update

# Base Packages
add-apt-repository universe

apt-get install -y --force-yes build-essential curl fail2ban gcc git libmcrypt4 libpcre3-dev \
make python2.7 python-pip sendmail supervisor ufw unattended-upgrades unzip whois zsh ncdu
pip install httpie


# Set The Timezone

# ln -sf /usr/share/zoneinfo/UTC /etc/localtime
ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Create The Root SSH Directory If Necessary

if [ ! -d /root/.ssh ]
then
    mkdir -p /root/.ssh
    touch /root/.ssh/authorized_keys
fi

# Setup Freshwork User

useradd freshwork
mkdir -p /home/freshwork/.ssh
mkdir -p /home/freshwork/.freshwork
adduser freshwork sudo

# Setup Bash For Freshwork User

chsh -s /bin/bash freshwork
cp /root/.profile /home/freshwork/.profile
cp /root/.bashrc /home/freshwork/.bashrc

# Set The Sudo Password For Freshwork

PASSWORD=$(mkpasswd enlace360)
usermod --password $PASSWORD freshwork

# Create The Server SSH Key

ssh-keygen -f /home/freshwork/.ssh/id_rsa -t rsa -N ''

# Copy Source Control Public Keys Into Known Hosts File

ssh-keyscan -H github.com >> /home/freshwork/.ssh/known_hosts
ssh-keyscan -H bitbucket.org >> /home/freshwork/.ssh/known_hosts
ssh-keyscan -H gitlab.com >> /home/freshwork/.ssh/known_hosts

# Configure Git Settings

git config --global user.name "Enlace"
git config --global user.email "contacto@enlace360.cl"

# Setup freshwork Home Directory Permissions

chown -R freshwork:freshwork /home/freshwork
chmod -R 755 /home/freshwork
chmod 700 /home/freshwork/.ssh/id_rsa

ufw allow 22
ufw allow 80
ufw allow 443
ufw allow 6001
ufw --force enable

# Allow FPM Restart

echo "freshwork ALL=NOPASSWD: /usr/sbin/service php7.3-fpm reload" > /etc/sudoers.d/php-fpm
echo "freshwork ALL=NOPASSWD: /usr/sbin/service php7.2-fpm reload" >> /etc/sudoers.d/php-fpm
echo "freshwork ALL=NOPASSWD: /usr/sbin/service php7.1-fpm reload" >> /etc/sudoers.d/php-fpm
echo "freshwork ALL=NOPASSWD: /usr/sbin/service php7.0-fpm reload" >> /etc/sudoers.d/php-fpm
echo "freshwork ALL=NOPASSWD: /usr/sbin/service php5.6-fpm reload" >> /etc/sudoers.d/php-fpm
echo "freshwork ALL=NOPASSWD: /usr/sbin/service php5-fpm reload" >> /etc/sudoers.d/php-fpm

# Install Base PHP Packages


apt-get install -y --force-yes php7.3-cli php7.3-dev \
php7.3-pgsql php7.3-sqlite3 php7.3-gd \
php7.3-curl php7.3-memcached \
php7.3-imap php7.3-mysql php7.3-mbstring \
php7.3-xml php7.3-zip php7.3-bcmath php7.3-soap \
php7.3-intl php7.3-readline

# Install Composer Package Manager

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Misc. PHP CLI Configuration

sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.3/cli/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.3/cli/php.ini
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.3/cli/php.ini
sudo sed -i "s/;date.timezone.*/date.timezone = UTC/" /etc/php/7.3/cli/php.ini

# Configure Sessions Directory Permissions

chmod 733 /var/lib/php/sessions
chmod +t /var/lib/php/sessions


# Install Nginx & PHP-FPM

    apt-get install -y --force-yes nginx php7.3-fpm

systemctl enable nginx.service

# Generate dhparam File

openssl dhparam -out /etc/nginx/dhparams.pem 2048

# Tweak Some PHP-FPM Settings

sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.3/fpm/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.3/fpm/php.ini
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.3/fpm/php.ini
sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.3/fpm/php.ini
sed -i "s/;date.timezone.*/date.timezone = UTC/" /etc/php/7.3/fpm/php.ini

# Configure FPM Pool Settings

sed -i "s/^user = www-data/user = freshwork/" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s/^group = www-data/group = freshwork/" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s/;listen\.owner.*/listen.owner = freshwork/" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s/;listen\.group.*/listen.group = freshwork/" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s/;listen\.mode.*/listen.mode = 0666/" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s/;request_terminate_timeout.*/request_terminate_timeout = 60/" /etc/php/7.3/fpm/pool.d/www.conf

# Configure Primary Nginx Settings

sed -i "s/user www-data;/user freshwork;/" /etc/nginx/nginx.conf
sed -i "s/worker_processes.*/worker_processes auto;/" /etc/nginx/nginx.conf
sed -i "s/# multi_accept.*/multi_accept on;/" /etc/nginx/nginx.conf
sed -i "s/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 128;/" /etc/nginx/nginx.conf

# Configure Gzip

cat > /etc/nginx/conf.d/gzip.conf << EOF
gzip_comp_level 5;
gzip_min_length 256;
gzip_proxied any;
gzip_vary on;

gzip_types
application/atom+xml
application/javascript
application/json
application/rss+xml
application/vnd.ms-fontobject
application/x-font-ttf
application/x-web-app-manifest+json
application/xhtml+xml
application/xml
font/opentype
image/svg+xml
image/x-icon
text/css
text/plain
text/x-component;

EOF

# Disable The Default Nginx Site

rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
service nginx restart

# Install A Catch All Server

cat > /etc/nginx/sites-available/catch-all << EOF
server {
    return 404;
}
EOF

ln -s /etc/nginx/sites-available/catch-all /etc/nginx/sites-enabled/catch-all


# Restart Nginx & PHP-FPM Services

# Restart Nginx & PHP-FPM Services

#service nginx restart
service nginx reload

if [ ! -z "\$(ps aux | grep php-fpm | grep -v grep)" ]
then
    service php7.3-fpm restart > /dev/null 2>&1
    service php7.2-fpm restart > /dev/null 2>&1
    service php7.1-fpm restart > /dev/null 2>&1
    service php7.0-fpm restart > /dev/null 2>&1
    service php5.6-fpm restart > /dev/null 2>&1
    service php5-fpm restart > /dev/null 2>&1
fi

# Add freshwork User To www-data Group

usermod -a -G www-data freshwork
id freshwork
groups freshwork


curl --silent --location https://deb.nodesource.com/setup_10.x | bash -

apt-get update

sudo apt-get install -y --force-yes nodejs

npm install -g pm2
npm install -g gulp
npm install -g yarn

# Set The Automated Root Password

export DEBIAN_FRONTEND=noninteractive

debconf-set-selections <<< "mysql-community-server mysql-community-server/data-dir select ''"
debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password CHTEglezJ7bhvw4ahv3G"
debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password CHTEglezJ7bhvw4ahv3G"

# Install MySQL

apt-get install -y mysql-server

# Configure Password Expiration

echo "default_password_lifetime = 0" >> /etc/mysql/mysql.conf.d/mysqld.cnf

# Configure Access Permissions For Root & freshwork Users

sed -i '/^bind-address/s/bind-address.*=.*/bind-address = */' /etc/mysql/mysql.conf.d/mysqld.cnf
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "GRANT ALL ON *.* TO root@'100.10.10.10' IDENTIFIED BY 'CHTEglezJ7bhvw4ahv3G';"
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "GRANT ALL ON *.* TO root@'%' IDENTIFIED BY 'CHTEglezJ7bhvw4ahv3G';"
service mysql restart

mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "CREATE USER 'freshwork'@'100.10.10.10' IDENTIFIED BY 'CHTEglezJ7bhvw4ahv3G';"
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "GRANT ALL ON *.* TO 'freshwork'@'100.10.10.10' IDENTIFIED BY 'CHTEglezJ7bhvw4ahv3G' WITH GRANT OPTION;"
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "GRANT ALL ON *.* TO 'freshwork'@'%' IDENTIFIED BY 'CHTEglezJ7bhvw4ahv3G' WITH GRANT OPTION;"
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "FLUSH PRIVILEGES;"

# Create The Initial Database If Specified

mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "CREATE DATABASE freshwork CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
mysql --user="root" --password="CHTEglezJ7bhvw4ahv3G" -e "CREATE DATABASE actual CHARACTER SET utf8 COLLATE utf8_unicode_ci;"


# Install & Configure Redis Server

apt-get install -y redis-server
sed -i 's/bind 127.0.0.1/bind 0.0.0.0/' /etc/redis/redis.conf
service redis-server restart
systemctl enable redis-server
# Install & Configure Memcached

apt-get install -y memcached
sed -i 's/-l 127.0.0.1/-l 0.0.0.0/' /etc/memcached.conf
service memcached restart
# Install & Configure Beanstalk

apt-get install -y --force-yes beanstalkd
sed -i "s/BEANSTALKD_LISTEN_ADDR.*/BEANSTALKD_LISTEN_ADDR=0.0.0.0/" /etc/default/beanstalkd

if grep START= /etc/default/beanstalkd; then
    sed -i "s/#START=yes/START=yes/" /etc/default/beanstalkd
else
    echo "START=yes" >> /etc/default/beanstalkd
fi

service beanstalkd start
sleep 5
service beanstalkd restart

systemctl enable beanstalkd

# Configure Supervisor Autostart

systemctl enable supervisor.service
service supervisor start

# Configure Swap Disk

if [ -f /swapfile ]; then
    echo "Swap exists."
else
    fallocate -l 1G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo "/swapfile none swap sw 0 0" >> /etc/fstab
    echo "vm.swappiness=30" >> /etc/sysctl.conf
    echo "vm.vfs_cache_pressure=50" >> /etc/sysctl.conf
fi

# Setup Unattended Security Upgrades

cat > /etc/apt/apt.conf.d/50unattended-upgrades << EOF
Unattended-Upgrade::Allowed-Origins {
    "Ubuntu bionic-security";
};
Unattended-Upgrade::Package-Blacklist {
    //
};
EOF

cat > /etc/apt/apt.conf.d/10periodic << EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
EOF

echo "fastcgi_param   SCRIPT_FILENAME     $request_filename;" >> /etc/nginx/fastcgi_params

# Install Chromium
sudo apt-get install -y chromium-browser unclutter xdotool -y

# Prevent the screen to turn off
gsettings set org.gnome.desktop.lockdown disable-lock-screen 'true'
gsettings set org.gnome.desktop.session idle-delay 0
gsettings set org.gnome.desktop.screensaver lock-enabled false

# Install Applications
sudo -u freshwork bash install-apps.sh

# Configure pm2
pm2 startup
pm2 start /home/freshwork/actual-socket/server.js
pm2 save

cat /etc/nginx/sites-available/actual << EOF
server {
    listen 80;
    listen [::]:80;
    server_name localhost;
    root /home/freshwork/actual/public;

    # FORGE SSL (DO NOT REMOVE!)
    # ssl_certificate
    # ssl_certificate_key

    ssl_protocols TLSv1.2;
    ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/dhparams.pem;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    charset utf-8;

    # FORGE CONFIG (DO NOT REMOVE!)
    #include forge-conf/actual/server/*;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log /var/log/nginx/actual-access.log;
    error_log  /var/log/nginx/actual-error.log error;

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
EOF

ln -s /etc/nginx/sites-available/actual /etc/nginx/sites-enabled/actual
service nginx restart

echo 'SCRIPT FINISHED'
